function map = adjustMap(norm,map)
assert(length(norm) == size(map,1))
unormID = find(norm == 0);

tmp = map(unormID,2);
map(unormID,2) =  map(unormID,3);
map(unormID,3) = tmp;

tmp = map(unormID,5);
map(unormID,5) =  map(unormID,6);
map(unormID,6) = tmp;

end