clear;close all;
% test the advancing front program
global fig;
global isEdgeBasis;
global flag1 flag2

[N1,T1]=NewMesh(4);
[N2,T2]=NewMesh(5);
%
% [N1,T1]=NewMesh(1);
% [N1,T1]=RefineMesh(N1,T1);
% [N1,T1]=RefineMesh(N1,T1);
% N1(:,5:end)=N1(:,5:end)+(0.1*rand(size(N1(:,5:end)))-0.05);
% [N2,T2]=NewMesh(2);
% [N2,T2]=RefineMesh(N2,T2);
% [N2,T2]=RefineMesh(N2,T2);
% N2(:,5:end)=N2(:,5:end)+(0.1*rand(size(N2(:,5:end)))-0.05);

% N2 = Nb(1:2,:);N1=Na(1:2,:);
% T2 =Tb;T1=Ta;

% [unit numNode numTri NodeList TriList] = readTRI('faceB.tri');
flag1 = zeros(size(T1,1),3);
flag2 = zeros(size(T2,1),3);

for i=1:size(T1,1) 
    flag1(i,:) = isRightHandSide(N1,T1(i,1:3));% give normal directions
end
for i=1:size(T2,1) 
    flag2(i,:) = isRightHandSide(N2,T2(i,1:3));
end

fig=figure(1);
clf
set(fig,'DoubleBuffer','on');
PlotMesh(N1,T1,'b');
PlotMesh(N2,T2,'r');
isEdgeBasis = true;
M=InterfaceMatrix(N1,T1,N2,T2);
Mf=full(M);