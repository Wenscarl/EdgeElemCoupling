function coord = FaceExtraction(varargin)
global rootDIR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Oameed Noakoasteen                       %
%                                          %
%                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin == 2
    pathname = varargin{1};
    prjname = varargin{2};

% READ DATA FROM .tri FILE
path = sprintf('%s/%s',rootDIR,pathname);
[LP,NumP,LT,NumT]=ReadData(fullfile(path,prjname)); 

% GENERATE A VIEW OF TBE MESH
%{
patch('faces',LT,'vertices',LP,'marker','o','FaceColor','none') 
view(3)
%grid
%}

% DETERMINE THE PLANES (X & Y & Z) THAT EACH POINT BELONGS TO
[gsx,xLS,gsy,yLS,gsz,zLS]=Plane(LP,NumP);

% VIEW THE X & Y & Z PLANE DATA
%{
gsx, xLS, gsy, yLS, gsz, zLS
%}

% CREATE SEPATATE FILES FOR EACH PLANE OF THE X & Y & Z
[x1,x2,y1,y2,z1,z2]=TriList(LT,NumT,gsx,xLS,gsy,yLS,gsz,zLS);

% VIEW THE PLANES 
%{
figure
%axis([-5 5 -5 5 -5 5])
grid
patch('faces',x1(:,1:3),'vertices',LP,'marker','o','FaceColor','none')
view(3)

figure
%axis([-5 5 -5 5 -5 5])
grid
patch('faces',x2(:,1:3),'vertices',LP,'marker','o','FaceColor','none')
view(3)

figure
%axis([-5 5 -5 5 -5 5])
grid
patch('faces',y1(:,1:3),'vertices',LP,'marker','o','FaceColor','none')
view(3)

figure
%axis([-5 5 -5 5 -5 5])
grid
patch('faces',y2(:,1:3),'vertices',LP,'marker','o','FaceColor','none')
view(3)

figure
%axis([-5 5 -5 5 -5 5])
grid
patch('faces',z1(:,1:3),'vertices',LP,'marker','o','FaceColor','none')
view(3)

figure
%axis([-5 5 -5 5 -5 5])
grid
patch('faces',z2(:,1:3),'vertices',LP,'marker','o','FaceColor','none')
view(3)
%}          

% GLOBAL TO LOCAL CONVERSION
[X1,X2]=GtL(xLS,x1,x2);
[Y1,Y2]=GtL(yLS,y1,y2);
[Z1,Z2]=GtL(zLS,z1,z2);

% OUTPUT .Tri FILES

Num=0;
nametmp = prjname(1:end-4);
str=sprintf('%s',nametmp);

[fx1,fx2] = WriteOut(LP,gsx,xLS,X1,X2,Num,str);

[fy1,fy2] = WriteOut(LP,gsy,yLS,Y1,Y2,Num+2,str);
%
[fz1,fz2] = WriteOut(LP,gsz,zLS,Z1,Z2,Num+4,str);
%}

% OUTPUT TRIANGLE GLOBAL NUMBER

coord = [fx1 fx2 fy1 fy2 fz1 fz2];
else
    disp('Wrong input!');
end




















