function M=InterfaceMatrix(Na,Ta,flaga,eindexa,jindexa,Nb,Tb,flagb,eindexb,jindexb)
% NOTE: Tb is testing side(row),only j 
global isEdgeBasis;
global numEdgea;
global numEdgeb;
global rowdim coldim
% global flag1 flag2
% global jindexa jindexb eindexb norm_a norm_b
% M: output coupling matrix is dense
% INTERFACEMATRIX projection matrix for nonmatching triangular grids 
%   M=InterfaceMatrix(Na,Ta,Nb,Tb); takes two triangular meshes Ta
%   and Tb with associated nodal coordinates in Na and Nb and
%   computes the interface projection matrix M
%   it has nothing to do with the src/rev dof index

% Ta->Tb 

% coldim =  2*(NumberEdges(Na,Ta)+size(Ta,1))*2% e_a and j_a, 4 times means e and j
% rowdim =  2*(NumberEdges(Nb,Tb)+size(Tb,1)) % only j_b

bl=[1];                        % bl: list of triangles of Tb to treat
% find two touching tri to start bil = ?
flag = 0;
for ii =1:size(Ta,1)
    flag = isTouchingTri(Nb(:,Tb(1,1:3)),Na(:,Ta(ii,1:3)));
    if (flag==1)
        bil=[ii];
        break
    end
end
% 
% bil=[1];                       
bil = [ii];% bil: list of triangles Ta to start with
bd=zeros(size(Tb,1)+1,1);      % bd: flag for triangles in Tb treated 
bd(end)=1;                     % guard, to treat boundaries
bd(1)=1;                       % mark first triangle in b list.
if isEdgeBasis
    M=sparse(rowdim,coldim);
else
    M=sparse(size(Nb,2),size(Na,2));
end

while length(bl)>0
  bc=bl(1); bl=bl(2:end);      % bc: current triangle of Tb 
  al=bil(1); bil=bil(2:end);   % triangle of Ta to start with
  ad=zeros(size(Ta,1)+1,1);    % same as for bd
  ad(end)=1;%end is a boundary non-exist
  ad(al)=1; 
  n=[0 0 0];                   % triangles intersecting with neighbors

  while length(al)>0
    ac=al(1); al=al(2:end);    % take next candidate
    [P,nc,Mc]=Intersect(Nb(:,Tb(bc,1:3)),flagb(bc),Na(:,Ta(ac,1:3)),flaga(ac));
    if ~isempty(P)             % intersection found
        if isEdgeBasis==1
%             indexbj = jmapb(bc,1:8)+1;
%             indexaj = jmapa(ac,1:8)+1;
%             indexae = emapa(ac,1:8)+1;
%             indexa =[indexae indexaj]
            rowind = jindexb(bc,:);
            colind = [eindexa(ac,:) jindexa(ac,:)];
            M(rowind,colind) = M(rowind,colind) + Mc;
        else
            M(Tb(bc,1:3),Ta(ac,1:3))=M(Tb(bc,1:3),Ta(ac,1:3))+Mc;
        end
      t=Ta(ac,3+find(ad(Ta(ac,4:6))==0)); 
      al=[al t];               % add neighbors 
      ad(t)=1;
      n(find(nc>0))=ac;        % ac is starting candidate for neighbor  
    end
  end
  tmp=find(bd(Tb(bc,4:6))==0); % find non-treated neighbors
  idx=find(n(tmp)>0);          % take those which intersect
  t=Tb(bc,3+tmp(idx));
  bl=[bl t];                   % and add them
  bil=[bil n(tmp(idx))];       % with starting candidates Ta
  bd(t)=1;
end