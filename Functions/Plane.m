%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Oameed Noakoasteen                       %
%                                          %
%                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Gsx,XLS,Gsy,YLS,Gsz,ZLS]=Plane(LP,NumP)

exL=zeros(3,NumP);                     % EXCLUSION FLAG
xL=[];                                 % FILE FOR X PLANE
yL=[];                                 % FILE FOR Y PLANE
zL=[];                                 % FILE FOR Z PLANE
for i=1:NumP
    if exL(1,i)~=1
        j=0;
        l=[];
        for k=i:NumP
            if LP(k,1)==LP(i,1)
                j=j+1;
                l=[l k];
                exL(1,k)=1;
            end
        end
        if exL(1,i)==1
            xL=[xL LP(i,1) j l];      % STORE "VALUE", "NUMBER" AND "ID NUMBER"
        end                           % FOR EACH VALUE OF X PLANE
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    if exL(2,i)~=1
        j=0;
        l=[];
        for k=i:NumP
            if LP(k,2)==LP(i,2)
                j=j+1;
                l=[l k];
                exL(2,k)=1;
            end
        end
        if exL(2,i)==1
            yL=[yL LP(i,2) j l];     % STORE "VALUE", "NUMBER" AND "ID NUMBER"
        end                          % FOR EACH VALUE OF Y PLANE
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    if exL(3,i)~=1
        j=0;
        l=[];
        for k=i:NumP
            if LP(k,3)==LP(i,3)
                j=j+1;
                l=[l k];
                exL(3,k)=1;
            end
        end
        if exL(3,i)==1
            zL=[zL LP(i,3) j l];    % STORE "VALUE", "NUMBER" AND "ID NUMBER"
        end                         % FOR EACH VALUE OF Z PLANE
    end
end

% READ INTO PLANE FILES (FOR X & Y & Z)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sx=size(xL);
sy=size(yL);
sz=size(zL);

i=2;
gsx=[];
while i<=sx(2)
    gsx=[gsx xL(i)];
    i=i+xL(i)+2;
end
sgsx=size(gsx);
xLS=zeros(sgsx(2),max(gsx)+1);     % INITIALIZE LIST FOR THE TWO X PLANES

i=1;
j=1;
k=1;
while i<=sx(2)                     
    xLS(j,k)=xL(i);
    xLS(j,k+1:k+1+xL(i+1)-1)=xL(i+2:i+2+xL(i+1)-1);
    i=i+1+xL(i+1)+1;
    j=j+1;
end
%
[a,c]=find(xLS(:,1)==max(xLS(:,1)));
[b,d]=find(xLS(:,1)==min(xLS(:,1)));
XLS=zeros(2,max(length(xLS(a,:)),length(xLS(b,:))));
XLS(1,:)=xLS(a,:);
XLS(2,:)=xLS(b,:);
Gsx=[gsx(a) gsx(b)];
%}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i=2;
gsy=[];
while i<=sy(2)
    gsy=[gsy yL(i)];
    i=i+yL(i)+2;
end
sgsy=size(gsy);
yLS=zeros(sgsy(2),max(gsy)+1);    % INITIALIZE LIST FOR THE TWO Y PLANES

i=1;
j=1;
k=1;
while i<=sy(2)
    yLS(j,k)=yL(i);
    yLS(j,k+1:k+1+yL(i+1)-1)=yL(i+2:i+2+yL(i+1)-1);
    i=i+1+yL(i+1)+1;
    j=j+1;
end

[a,c]=find(yLS(:,1)==max(yLS(:,1)));
[b,d]=find(yLS(:,1)==min(yLS(:,1)));
YLS=zeros(2,max(length(yLS(a,:)),length(yLS(b,:))));
YLS(1,:)=yLS(a,:);
YLS(2,:)=yLS(b,:);
Gsy=[gsy(a) gsy(b)];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
i=2;
gsz=[];
while i<=sz(2)
    gsz=[gsz zL(i)];
    i=i+zL(i)+2;
end
sgsz=size(gsz);
zLS=zeros(sgsz(2),max(gsz)+1);   % INITIALIZE LIST FOR THE TWO Z PLANES

i=1;
j=1;
k=1;
while i<=sz(2)
    zLS(j,k)=zL(i);
    zLS(j,k+1:k+1+zL(i+1)-1)=zL(i+2:i+2+zL(i+1)-1);
    i=i+1+zL(i+1)+1;
    j=j+1;
end

[a,c]=find(zLS(:,1)==max(zLS(:,1)));
[b,d]=find(zLS(:,1)==min(zLS(:,1)));
ZLS=zeros(2,max(length(zLS(a,:)),length(zLS(b,:))));
ZLS(1,:)=zLS(a,:);
ZLS(2,:)=zLS(b,:);
Gsz=[gsz(a) gsz(b)];

end
