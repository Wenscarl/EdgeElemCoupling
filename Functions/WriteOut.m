%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Oameed Noakoasteen                       %
%                                          %
%                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [featLow,featUpp]=WriteOut(LP,gsx,xLS,x1,x2,Num,str)
    global rootDIR
    format long
    path = sprintf('%s/%s',rootDIR,'Output');
    filename=[sprintf('%s_%d',str,Num),'.tri'];
    fid=fopen(fullfile(path,filename),'w');
    fprintf(fid,'%d \n',1);
    fprintf(fid,'%d \n',gsx(1));
    for j=1:gsx(1)
        fprintf(fid,'%15.12f %15.12f %15.12f\n',LP(xLS(1,j+1),:)');
    end
    sx1=size(x1);
    fprintf(fid,'%d \n',sx1(1));
    fprintf(fid,'%d %d %d \n',x1(:,1:3)');
    fclose(fid);
    
    filename=[sprintf('%s_%d',str,Num),'.glbid'];
    
    fid=fopen(fullfile(path,filename),'w');
    fprintf(fid,'%d \n',x1(:,4)-1');
    
    switch Num
        case 0
            featLow = LP(xLS(1,2),1);
            featUpp = LP(xLS(2,2),1);
        case 2
            featLow = LP(xLS(1,2),2);
            featUpp = LP(xLS(2,2),2);
        case 4
            featLow = LP(xLS(1,2),3);
            featUpp = LP(xLS(2,2),3);
    end

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    filename=[sprintf('%s_%d',str,Num+1),'.tri'];
    fid=fopen(fullfile(path,filename),'w');
    fprintf(fid,'%d \n',1);
    fprintf(fid,'%d \n',gsx(2));
    for j=1:gsx(2)
        fprintf(fid,'%15.12f %15.12f %15.12f\n',LP(xLS(2,j+1),:)');
    end
    sx2=size(x2);
    fprintf(fid,'%d \n',sx2(1));
    fprintf(fid,'%d %d %d \n',x2(:,1:3)');
    fclose(fid);
    
    filename=[sprintf('%s_%d',str,Num+1),'.glbid'];
    fid=fopen(fullfile(path,filename),'w');
    fprintf(fid,'%d \n',x2(:,4)-1');
    
    
end