%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Oameed Noakoasteen                       %
%                                          %
%                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [LP,NumP,LT,NumT]=ReadData(F)

% DATA IMPORT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DATA=importdata(F,' ',2);
NumP=str2double(DATA.textdata(2,1));
LP=DATA.data(1:NumP,:);                %LIST OF POINTS
NumT=DATA.data(NumP+1,1);
LT=DATA.data(NumP+2:end,:)+1;          %LIST OF TRIANGLES

end
