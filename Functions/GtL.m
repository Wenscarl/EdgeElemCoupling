%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Oameed Noakoasteen                       %
%                                          %
%                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [X1,X2]=GtL(xLS,x1,x2)

sx1=size(x1);
for i=1:sx1(1)
    for j=1:3
    [a,b]=find(x1(i,j)==xLS(1,2:end));
    x1(i,j)=b;
    end
end

sx2=size(x2);
for i=1:sx2(1)
    for j=1:3
    [a,b]=find(x2(i,j)==xLS(2,2:end));
    x2(i,j)=b;
    end
end
X1=x1;
X1(:,1:3)=X1(:,1:3)-1;
X2=x2;
X2(:,1:3)=X2(:,1:3)-1;
end

    

