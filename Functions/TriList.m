%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Oameed Noakoasteen                       %
%                                          %
%                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x1,x2,y1,y2,z1,z2]=TriList(LT,NumT,gsx,xLS,gsy,yLS,gsz,zLS)

x1=[];
x2=[];
y1=[];
y2=[];
z1=[];
z2=[];

for i=1:NumT
    
    flag=zeros(1,3);
    for j=1:3
        for k=1:gsx(1)
            if LT(i,j)==xLS(1,k+1);
                flag(j)=1;
                break;
            end
        end
    end
    
    if flag==ones(1,3)
        x1=[x1; LT(i,:) i];
    else
        flag=zeros(1,3);
        for j=1:3
            for k=1:gsx(2)
                if LT(i,j)==xLS(2,k+1);
                    flag(j)=1;
                    break;
                end
            end
        end
        
        if flag==ones(1,3)
            x2=[x2; LT(i,:) i];
        else
            flag=zeros(1,3);
            for j=1:3
                for k=1:gsy(1)
                    if LT(i,j)==yLS(1,k+1);
                        flag(j)=1;
                        break;
                    end
                end
            end
            
            if flag==ones(1,3)
                y1=[y1; LT(i,:) i];
            else
                flag=zeros(1,3);
                for j=1:3
                    for k=1:gsy(2)
                        if LT(i,j)==yLS(2,k+1)
                            flag(j)=1;
                            break;
                            
                        end
                    end
                end
                
                if flag==ones(1,3)
                    y2=[y2; LT(i,:) i];
                else
                    flag=zeros(1,3);
                    for j=1:3
                        for k=1:gsz(1)
                            if LT(i,j)==zLS(1,k+1)
                                flag(j)=1;
                                break;
                            end
                        end
                    end
                    
                    if flag==ones(1,3)
                        z1=[z1; LT(i,:) i];
                    else
                        flag=zeros(1,3);
                        for j=1:3
                            for k=1:gsz(2)
                                if LT(i,j)==zLS(2,k+1)
                                    flag(j)=1;
                                    break;
                                end
                            end
                        end
                        
                        if flag==ones(1,3)
                            z2=[z2; LT(i,:) i];
                        end
                    end
                end
            end
        end
    end
end

end
