function elemMat=MortarIntM2(T,X,Y)
% MORTARINT computes mortar contributions 
%   M=MortarInt(T,X,Y); computes for triangles X and Y with nodal coordinates 
%   stored columnwise the integrals of all P1 element shape function combinations 
%   between triangles X and Y on triangle T.
%   The result is stored in the 3 by 3 matrix M 

elemMat = zeros(3,3);
T_3d = [T;1 1 1];
X_3d = [X; 1 1 1];
Y_3d = [Y; 1 1 1];
area = 0.5*norm(cross((T_3d(:,1)-T_3d(:,2)),(T_3d(:,1)-T_3d(:,3))),2);
area = polyarea(T(1,:),T(2,:),2);
quadPoints=13;
for i=0:2
    for j=0:2
        entry = 0.;
        for k=0:quadPoints-1
            [zetaT weight] =  GetFormular(quadPoints, k);
            pt = T*zetaT';
            zeta1 = PointInFace(pt, X);
            zeta2 = PointInFace(pt, Y);
            w1 = basisphi(i,zeta1,X_3d);%X and Y need to be used
            w2 = basisphi(j,zeta2,Y_3d);
            entry = entry + area * weight * (w1*w2);
        end
        elemMat(i+1,j+1) = entry;
    end
end