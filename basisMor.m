function w = basisMor(p,zeta,T,norm)% T is the triangle
% this is self (src)
if(norm==1)
FACE_BASIS_MOR_IJK = [1, 2, -1
	1, 2, -1
	0, 2, -1
	0, 2, -1
	0, 1, -1
	0, 1, -1
	0, 1,  2
	0, 1,  2];
else
    FACE_BASIS_MOR_IJK =[2,1, -1 
	2, 1, -1
	0, 1, -1
	0, 1, -1
	0, 2, -1
	0, 2, -1
	0, 2,  1
	0, 2,  1];
end



gradZeta = zeros(3,3);% gradZeta1x gradZeta1y gradZeta1z ...
[gradZeta area] = geometrynew(T); % T is 3d coord

gradZeta=gradZeta/(2.*area);

i = FACE_BASIS_MOR_IJK(p+1,1)+1;% +1 for MATLAB
j = FACE_BASIS_MOR_IJK(p+1,2)+1;
k = FACE_BASIS_MOR_IJK(p+1,3)+1;

switch p
	case 0
    	w = gradZeta(j,:)*zeta(i) - gradZeta(i,:)*zeta(j);
    case 2
		w = gradZeta(j,:)*zeta(i) - gradZeta(i,:)*zeta(j);
    case 4
    	w = gradZeta(j,:)*zeta(i) - gradZeta(i,:)*zeta(j);
	case 1
		w = (gradZeta(j,:)*zeta(i) + gradZeta(i,:)*zeta(j)) * 4.;
	case 3
		w = (gradZeta(j,:)*zeta(i) + gradZeta(i,:)*zeta(j)) * 4.;
	case 5
		w = (gradZeta(j,:)*zeta(i) + gradZeta(i,:)*zeta(j)) * 4.;
	case 6
		w = (gradZeta(k,:)*zeta(i) - gradZeta(i,:)*zeta(k)) * zeta(j) * 4.;
	case 7
		w = (gradZeta(j,:)*zeta(i) - gradZeta(i,:)*zeta(j)) * zeta(k) * 4.;
    otherwise
        disp('Wrong Basis index.');
end

