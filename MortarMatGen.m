function varargout = MortarMatGen(varargin)    
    % test the advancing front program
    global fig;
    global isEdgeBasis;
    global numEdgea;
    global numEdgeb;
if nargin == 3
    srcName = varargin{1};
    rcvName = varargin{2};
    basisType = varargin{3};
    if basisType=='e'
        isEdgeBasis = 1;
    else
        isEdgeBasis = 0;
    end
        
    [unit1 numNode1 numTri1 NodeList1 TriList1] = readTRI(srcName);
    [unit2 numNode2 numTri2 NodeList2 TriList2] = readTRI(rcvName);
    
    [Ta Na numEdgea] = appendNeighbour(unit1 ,numNode1, numTri1 ,NodeList1 ,TriList1);
    [Tb Nb numEdgeb] = appendNeighbour(unit2 ,numNode2, numTri2 ,NodeList2 ,TriList2);
    
    N2 = Nb(1:2,:);
    N1 = Na(1:2,:);
    T2 = Tb;
    T1 = Ta;

    flag1 = zeros(size(T1,1),3);
    flag2 = zeros(size(T2,1),3);

    for i=1:size(T1,1) 
        flag1(i,:) = isRightHandSide(N1,T1(i,1:3));
    end
    for i=1:size(T2,1) 
        flag2(i,:) = isRightHandSide(N2,T2(i,1:3));
    end

    fig=figure(1);
    clf
    set(fig,'DoubleBuffer','on');
    PlotMesh(N1,T1,'b');
    PlotMesh(N2,T2,'r');

    
    [ emap1 jmap1 norm1] = loadinterfaceData('interface5.id');
    [ emap2 jmap2 norm2] = loadinterfaceData('interface5.id');
    M = InterfaceMatrix(N2,T2,norm2,emap2,jmap2,N1,T1,norm1,emap1,jmap1);

    Mf=full(M);
    varargout{1} = Mf;
end