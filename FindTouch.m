function retVal = FindTouch(varargin)
eps = 1e-9;
if nargin == 3
    pathname = varargin{1};
    prj1name = varargin{2};
    prj2name = varargin{3};
    y1 = FaceExtraction(pathname ,prj1name);
    y2 = FaceExtraction(pathname ,prj2name);
    
    y = [y1;y2];
    touchID = [];
    for i = 1:2:5
        if abs(y(1,i)-y(2,i+1))<eps 
            touchID = [i-1,i];
            break
        end
        
        if abs(y(1,i+1)-y(2,i))<eps
            touchID = [i,i-1];
            break
        end
    end
   
    switch floor(sum(touchID)/4)
        case 0
            retVal.dir = 'x';
        case 1
            retVal.dir = 'y';
        case 2
            retVal.dir = 'z';
    end
    retVal.id = touchID;
else
    disp('Error Input!');
end
end