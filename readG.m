% clear;
close all;
% G = importdata('../NCDDMrun/Weightbox/ABbox/G21_matrix.txt');
% G_row = importdata('../NCDDMrun/Weightbox/ABbox/G21_row.txt');G_row = G_row+1;
% G_col = importdata('../NCDDMrun/Weightbox/ABbox/G21_col.txt');G_col = G_col+1;

G = importdata('../NCDDMrun/Litebox/ABbox/G12_matrix.txt');
G_row = importdata('../NCDDMrun/Litebox/ABbox/G12_row.txt');G_row = G_row+1;
G_col = importdata('../NCDDMrun/Litebox/ABbox/G12_col.txt');G_col = G_col+1;


G_real = csr_to_sparse(G_row,G_col,G(:,1));
G_imag = csr_to_sparse(G_row,G_col,G(:,2));
Gf = G_real+j*G_imag;
spy(Gf);

% Gf(abs(Gf)<1e-14)=0+0*j;

% Gf=sparse(Gf);
% subplot(1,3,1);

% subplot(1,3,2);
% spy(Gf+conj(Gf));
% subplot(1,3,3);
% spy(Gf-conj(Gf));
figure(2);surf(abs(full(Gf(:,1:76)-M)))