function phi = basisphiMor(p,zeta,T)% T is the triangle
% FACE_BASIS_MOR_IJK = [1, 2, -1
% 	1, 2, -1
% 	0, 2, -1
% 	0, 2, -1
% 	0, 1, -1
% 	0, 1, -1
% 	0, 1,  2
% 	0, 1,  2];


FACE_BASIS_MOR_IJK =[1, 2, -1 % this is fake 
	0, 2, -1
	0, 1, -1
	1, 2, -1
	0, 2, -1
	0, 1, -1
	0, 1,  2
	0, 1,  2];
gradZeta = zeros(3,3);% gradZeta1x gradZeta1y gradZeta1z ...
[gradZeta area] = geometrynew(T); % T is 3d coord

gradZeta=gradZeta/(2.*area);

i = FACE_BASIS_MOR_IJK(p+1,1)+1;% +1 for MATLAB
j = FACE_BASIS_MOR_IJK(p+1,2)+1;
k = FACE_BASIS_MOR_IJK(p+1,3)+1;

% first order nodal basis
switch p
    case 0
        phi = zeta(1);
    case 2
        phi = zeta(2);
    case 1
        phi = zeta(3);
    otherwise
        disp('Wrong Basis index.');
end

% switch p
% 	case 0
%         phi = 2.*zeta(1)*(zeta(1) - 0.5);
%     case 1
% 		phi = 2.*zeta(2)*(zeta(2) - 0.5);
%     case 2
%     	phi = 2.*zeta(3)*(zeta(3) - 0.5);
% 	case 3
% 		phi = 4.*(zeta(2)*zeta(3));
% 	case 4
% 		phi = 4.*(zeta(1)*zeta(3));
% 	case 5
% 		phi = 4.*(zeta(1)*zeta(2));
%     otherwise
%         disp('Wrong Basis index.');
% end
