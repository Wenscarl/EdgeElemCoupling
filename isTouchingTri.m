function flag = isTouchingTri(X,Y)
[P,n]=EdgeIntersections(X,Y);
P1=PointsOfXInY(X,Y);                             % neighbors
P=[P P1];
P=[P PointsOfXInY(Y,X)];
P=SortAndRemoveDoubles(P);           % sort counter clock wise
if size(P,2)>0
    flag = 1;
else 
    flag = 0;
end