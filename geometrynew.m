function [gradZeta area] = geometrynew(T)
% T:  x1 x2 x3
%     y1 y2 y3
%     z1 z2 z3 the 3rd dim is needed for cross in MATLAB

t0 = T(:,3)-T(:,2);
t1 = T(:,1)-T(:,3);
t2 = T(:,2)-T(:,1);
normal = cross(t0,t1);
area = 0.5*norm(normal,2);
normal = normal/norm(normal,2);

gradZeta=zeros(3,3);
gradZeta(1,:)=cross(normal,t0);
gradZeta(2,:)=cross(normal,t1);
gradZeta(3,:)=cross(normal,t2);

if gradZeta(1,:)*t1 < 0.0
    
	gradZeta(1,:) = gradZeta(1,:) * -1.;	
end
if gradZeta(2,:)*t0 > 0.0
	gradZeta(2,:) = gradZeta(2,:) * -1.;
end	
if gradZeta(3,:)*t0 < 0.0 
	gradZeta(3,:) = gradZeta(3,:) * -1.0;
end