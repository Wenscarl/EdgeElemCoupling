function Mc = compress(M)
[m,n] = size(M);
rowNULL = zeros(1,n);

rmv_row = [];
for i = 1:m-1

    if ~isequal(M(i,:),rowNULL)
        rmv_row = [rmv_row i];
    else
        continue;
    end
end
M = M(rmv_row,:);
[m n]

[m,n] = size(M);
colNULL = zeros(m,1);
rmv_col = [];
for i = 1:n-1
    if ~isequal(M(:,i),colNULL)
        rmv_col = [rmv_col i];
    else
        continue;
    end
end
Mc = M(:,rmv_col);
[m n]
[m,n] = size(Mc)
end