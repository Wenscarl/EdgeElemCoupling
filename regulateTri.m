function [Nr,Tr,flag]=regulateTri(normal,N,T)
% N : input nodes info, each col is a node
% T : input Tri info
% Tr: return Tri file
 Tr = zeros(size(T));
 numTri = size(T,1);
 flag = zeros(numTri,1); % num of tri
 
for i = 1:numTri
    eg1 = N(:,T(i,2))- N(:,T(i,1));
    eg2 = N(:,T(i,3))- N(:,T(i,1));
    eg1 = expandto3D(eg1,normal);
    eg2 = expandto3D(eg2,normal);
    nvtr = cross(eg1,eg2);
    nvtr = nvtr./(norm(nvtr,2));
    if isequal(nvtr,normal')
        flag(i)=true;
        Tr(i,:) = T(i,:) ;
    else
        flag(i) = false;
        Tr(i,:) = T(i,[1 3 2 6 5 4]) ;
    end

end;
    Nr = N;
end

function eg = expandto3D(eg,normal)

if isequal(normal,[1 0 0])
    eg = [0; eg];
elseif isequal(normal,[0 1 0])
    eg = [eg(1);0; eg(2)];
elseif isequal(normal,[0 0 1])
    eg = [ eg;0];
else
      disp('Error, no such normal vector is defined! Try again!') 
end
end