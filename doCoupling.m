clear;close all;
global rootDIR
global isEdgeBasis;
% test the advancing front program
global fig;
% global flag1 flag2 % indicate two tri's normal consistancy
% global jindexa jindexb eindexb norm_a norm_b
isEdgeBasis=true;
rootDIR = '/home/wang/workspace/MortarCoupling';
% rootDIR = '/home/ian/Documents/MortarCoupling/Working';
addpath(genpath(rootDIR));

receive = FindTouch('TriFiles','boxA.tri','boxB.tri');
skip = receive.dir;
touchid = receive.id;

%% readin geometry info
srcName = sprintf('%s_%d','boxA',touchid(1));
rcvName = sprintf('%s_%d','boxB',touchid(2));
[unitB numNodeB numTriB NodeListB TriListB globIdB] = readTRI('Output',rcvName,true);
[unitA numNodeA numTriA NodeListA TriListA globIdA] = readTRI('Output',srcName,true);
%% test 
% oriName = sprintf('%s','boxA');
% [unit numNode numTri NodeList TriList] = readTRI('TriFiles',oriName,false);
% 
% NodeListA(TriListA(11,:)+1)
% NodeList(TriList(globIdA(11)+1,:)+1)
%%
assert(unitA == unitB);
[Na,Ta,numE1] = mapto2d(NodeListA,TriListA,skip);
[Nb,Tb,numE2] = mapto2d(NodeListB,TriListB,skip);

%% read in dof id need to change
[ emap1 jmap1 norm1] = loadinterfaceData('TriFiles','boxA'); % norm1,2 change the sequence of ejmap
[ emap2 jmap2 norm2] = loadinterfaceData('TriFiles','boxB');

% eindexa = emap1(globIdA+1,:)+1;
% jindexa = jmap1(globIdA+1,:)+1;
% norm_a = norm1(globIdA+1); 
% eindexa = adjustMap(norm_a,eindexa);
% jindexa = adjustMap(norm_a,jindexa);
% 
% jindexb = jmap2(globIdB+1,:)+1;
% norm_b = norm2(globIdB+1);
% jindexb = adjustMap(norm_b,jindexb);
%%
% this is for G21
jindexb = [2 3 4 5 6 7 0 1;10 11 12 13 6 7 8 9]+1;

jindexa = [16 17 18 19 20 21 14 15; 24 25 16 17 26 27 22 23]+1;
eindexa = [2 3 4 5 6 7 0 1;10 11 2 3 12 13 8 9]+1;

% this is for G12
% jindexa = [2 3 4 5 6 7 0 1;10 11 2 3 12 13 8 9]+1;
% 
% jindexb = [16 17 18 19 20 21 14 15; 24 25 26 27 20 21 22 23]+1;
% eindexb = [2 3 4 5 6 7 0 1;10 11 12 13 6 7 8 9]+1;

% pre-known info, common normal direction
switch skip
  case char('x')
    normal = [1,0,0];
  case char('y')
    normal = [0,1,0];
  case char('z')
   normal = [0,0,1];
  otherwise
      disp('Error, no such touching id is found! Try again!') 
end


% [N1,T1,flag1]=regulateTri(normal,Na,Ta);
% [N2,T2,flag2]=regulateTri(normal,Nb,Tb);
% this code works for T1, T2 have the same normal direction
%%

fig=figure(1);
clf
set(fig,'DoubleBuffer','on');
[N1,T1]=NewMesh(6);
[N2,T2]=NewMesh(7);
[N1,T1,flag1]=regulateTri(normal,N1,T1);% change the order of T1 according to normal, make it as normal!!
[N2,T2,flag2]=regulateTri(normal,N2,T2);

PlotMesh(N1,T1,'b');
PlotMesh(N2,T2,'r');
% T2(e,j)->T1(j)
% M = InterfaceMatrix(N2,T2,flag2,eindexb,jindexb,N1,T1,flag1,jindexa);
M = InterfaceMatrix(N1,T1,flag1,eindexa,jindexa,N2,T2,flag2,jindexb);
Mf=full(M);
% M_compress = sparse(compress(Mf));
% figure(2)
% spy(M_compress)