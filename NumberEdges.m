function numEdge = NumberEdges(N,TriList)
A = 0.0*sparse(size(N,2),size(N,2));
numTri = size(TriList,1);

for i=1:numTri
    edge1 = sort(TriList(i,1:2));
    edge2 = sort(TriList(i,2:3));
    edge3 = sort(TriList(i,[1,3]));
    edgeLocalList = [edge1;edge2;edge3];
    for ii =1: 3
       A(edgeLocalList(ii,1),edgeLocalList(ii,2)) =1;
    end
end;
numEdge = nnz(A);
