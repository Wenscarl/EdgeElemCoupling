function [N,T,numEdge] = mapto2d(NodeList,TriList, skip)
% input :NodeList,TriList are in the format of tri file
% input :skip is the overlapping coordinate which can be map to 2d
numTri = size(TriList,1);
switch skip
  case char('x')
      fprintf('Touching in X-direction.\n')
      N(1,:) = NodeList(:,2);
      N(2,:) = NodeList(:,3);
  case char('y')
      fprintf('Touching in Y-direction.\n')
      N(1,:) = NodeList(:,1);
      N(2,:) = NodeList(:,3);
  case char('z')
      fprintf('Touching in Z-direction.\n')
      N(1,:) = NodeList(:,1);
      N(2,:) = NodeList(:,2);
  otherwise
      disp('Error, no such touching id is found! Try again!') 
end


T = zeros(numTri,6);
T(:,1:3) = TriList;
BoundFlag = numTri+1;
numEdge = 0;

EdgeName =  [];
Edgetri1  = [];
Edgetri2  = [];
for i = 1:numTri
    edge1 = sort(TriList(i,1:2));
    edge2 = sort(TriList(i,2:3));
    edge3 = sort(TriList(i,[1,3]));
    edgeLocalList = [edge1;edge2;edge3];
    for j = 1:3
        key = [edgeLocalList(j,1) edgeLocalList(j,2)];
        if(~isempty(EdgeName))
            ri = find(EdgeName(:,1)==key(1) & EdgeName(:,2)==key(2));
        else
            ri = [];
        end
        if isempty(ri)
            EdgeName = [EdgeName;key];
            Edgetri1 = [Edgetri1; [i,j]];
            Edgetri2 = [Edgetri2; [-1 -1]];
            numEdge = numEdge+1;
        else
            Edgetri2(ri,:) = [i,j];
        end
    end
end
% EdgeName
% Edgetri1
% Edgetri2

for i = 1:numEdge
    tri1Id = Edgetri1(i,1);
    tri1LocId = Edgetri1(i,2);
    tri2Id = Edgetri2(i,1);
    tri2LocId = Edgetri2(i,2);
    if tri2Id == -1
        tri2Id = BoundFlag;
    end
    
    T(tri1Id,tri1LocId+3) = tri2Id;
    if tri2Id == BoundFlag
        continue;
    else
        T(tri2Id,tri2LocId+3) = tri1Id;
    end
    
end
T(:,1:3) = T(:,1:3)+1;
end
