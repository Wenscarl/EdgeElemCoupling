function [ emap jmap norm] = loadinterfaceData(pathname,filename)
global rootDIR

IntIDfileName = sprintf('%s_interface.id',filename);
% fid=fopen(fullfile(pathname,IntIDfileName),'r');

A = load(IntIDfileName);

norm = A(:,2);
emap = A(:,3:10);
jmap = A(:,11:18);
end

% for i =1:size(A,1)
%     if(norm(i)==0)
%         tmp = emap(i,3:4) ;
%         emap(i,3:4) = emap(i,5:6);
%         emap(i,5:6) = tmp;
%         
%         tmp = jmap(i,3:4);
%         jmap(i,3:4) = jmap(i,5:6);
%         jmap(i,5:6) = tmp;
%     end
% end
