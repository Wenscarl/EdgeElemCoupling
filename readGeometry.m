function [NdList TriList jIdxArr eIdxArr] = readGeometry(filename)
IntGeofileName = sprintf('%s.geo',filename);
fid=fopen(IntGeofileName,'r');
NdList = [];
TriList = [];
jIdxArr = [];
eIdxArr = [];
NumTri = 0;
lenNd = 0;

% to start up
start = [];
for i = 1:3
    Pt = fscanf(fid,'%f %f %f', [1 3]);
    NdList = [NdList;Pt];
    start = [start i];
end
TriList = [TriList; start];
jIdx = fscanf(fid,'%d %d %d', [1 8]);
eIdx = fscanf(fid,'%d %d %d', [1 8]);
jIdxArr = [jIdxArr ;jIdx];
eIdxArr = [eIdxArr ;eIdx];
lenNd = length(NdList(:,1));

while ~feof(fid)
    locTri = zeros(1,3);
    for i = 1:3
        Pt = fscanf(fid,'%f %f %f', [1 3]);
        ri = find(Pt(1)==NdList(:,1) & Pt(2)==NdList(:,2) &Pt(3)==NdList(:,3) );

        if (isempty(ri))
            NdList = [NdList;Pt];
            lenNd = lenNd +1;
            locTri(i) = lenNd;
        else
            locTri(i) = ri;
        end
        
    end
    TriList = [TriList;locTri];
        
    jIdx = fscanf(fid,'%d %d %d', [1 8]);
    eIdx = fscanf(fid,'%d %d %d', [1 8]);
    jIdxArr = [jIdxArr ;jIdx];
    eIdxArr = [eIdxArr ;eIdx];
end
    
fclose(fid);
TriList = TriList -1;
end