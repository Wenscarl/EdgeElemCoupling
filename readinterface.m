function [globalid IntNdList interfaceTri] = readinterface(filename,triName,output)
A = load(filename);
globalid = A(:,1);
norm = A(:,2);
emap = A(:,3:10);
jmap = A(:,11:18);

[unit numNode numTri NodeList TriList] = readTRI(triName);

IntNDIndex=[];
interfaceTriNum = size(globalid,1);
interfaceTri = zeros(interfaceTriNum,3);
for i=1:interfaceTriNum
    interfaceTri(i,:) = TriList(globalid(i)+1,:);% globalid is 0 based
    for j=1:3
        localND = interfaceTri(i,j);
        if find(IntNDIndex==localND)>0
            continue;
        else
            IntNDIndex = [IntNDIndex;localND];
        end
    end
end

numIntNd = size(IntNDIndex,1);
IntNDIndex = [IntNDIndex (0:numIntNd-1)'];
IntNdList = zeros(numIntNd,3);
for i =1:numIntNd
    IntNdList(i,:) = NodeList(IntNDIndex(i,1)+1,:);
end

for i =1 :interfaceTriNum
    for j=1:3
        id = interfaceTri(i,j);
        
        interfaceTri(i,j) = IntNDIndex(find(IntNDIndex(:,1)==id),2);
    end
end
fid=fopen(output,'w');
fprintf(fid,'%s\n',num2str(unit));
fprintf(fid,'%d\n',numIntNd);
for i=1:numIntNd
    fprintf(fid, '%22.20e %22.20e %22.20e\n',IntNdList(i,1),IntNdList(i,2),IntNdList(i,3));
end
fprintf(fid,'%d\n',interfaceTriNum);
for i=1:interfaceTriNum
    fprintf(fid, '%d %d %d\n',interfaceTri(i,1),interfaceTri(i,2),interfaceTri(i,3));
end
fclose(fid);


