function [N,T]=NewMesh(p)
% NEWMESH generates simple triangular meshes 
%   [N,T]=NewMesh(p); generates an initial coarse triangular mesh for
%   each value of p. The result is a list of triangles T which points
%   into the list of nodes N containing x and y coordinates.  The
%   triangle contains in entries 4 to 6 the neighboring triangle
%   indices, and as a guard # of triangles + 1 if there is no
%   neighbor.

if p==1
  N=[0 1 0 1
     0 0 1 1];

  T=[1 2 4 3 3 2
     1 4 3 1 3 3];
elseif p==2
  N=[0 1 0 1 0.5
     0 0 1 1  1];

  T=[1 2 5 4 2 3
     2 4 5 4 4 1
     1 5 3 1 4 4];  
elseif p==3
  N=[0 1 0
     0 0 1];
  T=[1 2 3 2 2 2];  
%   ian added
elseif p==4
    N=[0,0.5,0.5,1,1,0.5,1,0,0;
        0.5,0,2/3,0,0.5,1,1,1,0];
   T = [0,1,2,5,6,4;
       3,4,1,9,6,9;
       5,2,6,7,8,9;
       0,2,7,1,7,9;
       0,8,1,9,9,1;
       1,4,2,2,8,1;
       2,5,7,3,9,4;
       2,4,6,6,9,3];
   T(:,1:3)=T(:,1:3)+1;
elseif p==5
    N= [0.5000    0.5000    1.0000    1.0000    1.0000         0         0    0.5000         0;
    1.0000    0.5000    0.5000         0    1.0000    1.0000    0.5000         0         0];
    
    T=[0,1,2,2,4,5;
        0,5,1,9,6,1;
        1,7,3,8,9,4;
        3,2,1,9,1,3;
        4,0,2,9,1,9;
        5,6,1,9,8,2;
        6,8,7,9,9,8;
        6,7,1,7,3,6];
    T(:,1:3)=T(:,1:3)+1;
elseif p==6
    h=0.15;
    N=[-h -h h h;-h h h -h];
    T =[0 3 1,3,2,3;
        3 2 1,3,3,1];
    T(:,1:3)=T(:,1:3)+1;
elseif p==7
        h=0.15;
    N = [h -h h -h;h h -h -h];
    T = [3 0 1,2 3 3;
        3 0 2,1,3,3];
    T(:,1:3)=T(:,1:3)+1;
else
  N=[0 1 0 1
     0 0 1 1];

  T=[1 2 3 3 2 3
     2 4 3 3 3 1];
end

