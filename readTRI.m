function [unit numNode numTri NodeList TriList globId] = readTRI(pathname,prjname, isSplit)

TrifileName = sprintf('%s.tri',prjname);
fid=fopen(fullfile(pathname,TrifileName),'r');
unit = str2num(fgetl(fid));
numNode = str2num(fgetl(fid));

NodeList=[];
numtmp=str2num(fgetl(fid));% numtmp is a line of coord (x,y,z)
NodeList = numtmp;

ndCnt = 0;
while(ndCnt<numNode-1)
    NodeList = [NodeList;str2num(fgetl(fid))];
    ndCnt = ndCnt + 1;
end

numTri = str2num(fgetl(fid));
while( size(numTri,2)~=1)
    numTri = str2num(fgetl(fid));
end


TriList=[];
cnt = numTri;
%  read in tri
while(cnt)
    numtmp =str2num(fgetl(fid));
    TriList = [TriList;numtmp];
    cnt= cnt-1;
end
fclose(fid);

if(isSplit)
globalIdName = sprintf('%s.glbid',prjname);
globId = importdata(globalIdName);
assert(length(globId) == numTri);

else
    globId = [];
end
end