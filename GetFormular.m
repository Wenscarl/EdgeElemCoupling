function [zeta weight] =  GetFormula(np, id)
  zeta = zeros(1,3);
  if (id < 0 || id >= np) 
      disp('Wrong id!');
      weight = 999.;
      zeta=[999.,999.,999.];
  else
      repo = [1 3 4 6 7 9 12 13 16];
      if(size(find(repo==np),2)==0)
          weight = 0.0;
          zeta=[0.,0.,0.];
          disp('Wrong id!');
      else
          g2d = gaussParameter(np);
          zeta = g2d(id+1,1:end-1);
          weight =g2d(id+1,end);
      end
  end