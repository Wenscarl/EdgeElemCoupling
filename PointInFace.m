function zeta = PointInFace(pt, face)
Ae = polyarea(face(1,:),face(2,:),2);
A1 = polyarea([face(1,[2 3]) pt(1)],[face(2,[2 3]) pt(2)],2);
A2 = polyarea([face(1,[1 3]) pt(1)],[face(2,[1 3]) pt(2)],2);
A3 = polyarea([face(1,[2 1]) pt(1)],[face(2,[2 1]) pt(2)],2);

zeta = [A1 A2 A3] / Ae;