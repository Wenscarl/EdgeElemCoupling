function elemMat=MortarIntEdgeElement(T,X,normX,Y,normY)
% MORTARINT computes mortar contributions 
%   M=MortarInt(T,X,Y); computes for triangles X and Y with nodal coordinates 
%   stored columnwise the integrals of all P1 element shape function combinations 
%   between triangles X and Y on triangle T.
%   The result is stored in the 3 by 3 matrix M 
global cj
k0 = 2*pi/2.9979e8*200e6;
%% Transmission condition parameter for FOTC only a
a = -cj*k0;

%%
kkkkk = 0;
elemMatTjj = zeros(8,8);

T_3d = [T;0 0 0];
X_3d = [X; 0 0 0];
Y_3d = [Y; 0 0 0];
% area = 0.5*norm(cross((T_3d(:,1)-T_3d(:,2)),(T_3d(:,1)-T_3d(:,3))),2);
area = polyarea(T(1,:),T(2,:),2);
quadPoints=6;
for k=0:quadPoints-1
    [zetaT weight] =  GetFormular(quadPoints, k);
    pt = T*zetaT';
%     if(norm(pt -[-0.08333;-0.05])< 0.001)
%         kkkkk =1;
%     end
%     elemTjj_one_pt = zeros(8,8);
    for i=0:7
        for j=0:7
                zeta1 = PointInFace(pt, X);
                zeta2 = PointInFace(pt, Y);
                w1 = basisMor(i,zeta1,X_3d,normX);%X and Y need to be used
                w2 = basis(j,zeta2,Y_3d,normY);
                entry = area * weight * (w1*w2');
            Tjj_quad_pt(i+1,j+1) =  entry;
%             if(kkkkk == 1)
%                 [w1;w2]
%                 [i j entry]
%                 elemTjj_one_pt
%                 dmm = 66;
%             end
        end
    end
    elemMatTjj = elemMatTjj + Tjj_quad_pt;
end
TjjCoeff      = -k0 * k0 / a;
TjeCoeff      =  k0;
elemMatTje = elemMatTjj;
elemMat = [elemMatTje*TjeCoeff elemMatTjj*TjjCoeff];

end
