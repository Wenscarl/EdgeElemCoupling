function isCW = isRightHandSide(N,index)
P = N(:,index);
P1 = P(:,1);
P2 = P(:,2);
P3 = P(:,3);

t1 = P2-P1;
t2 = P3-P1;
t1=[t1;0];
t2=[t2;0];

isCW = cross(t1,t2);

isCW = isCW/norm(isCW,2);
    