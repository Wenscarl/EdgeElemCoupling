clear;close all;
global isEdgeBasis;
% test the advancing front program
global fig;
global flag1 flag2
global jindexa jindexb eindexa

isEdgeBasis=true;

[Na,Ta]=NewMesh(6);
[Nb,Tb]=NewMesh(7);
eindexa = [2 3 4 5 6 7 0 1; 10 11  4 5 12 13 8 9]+1;
jindexa = [16 17 18 19 20 21 14 15;24 25 18 19 26 27 22 23]+1;

jindexb = [2 3 4 5 6 7 0 1; 10 11 12 13 2 3 8 9]+1;

normal = [0;0;1];% pre-known info, common normal direction
[N1,T1,flag1]=regulateTri(normal,Na,Ta);
[N2,T2,flag2]=regulateTri(normal,Nb,Tb);
% this code works for T1, T2 have the same normal direction


fig=figure(1);
clf
set(fig,'DoubleBuffer','on');
PlotMesh(N1,T1,'b');
PlotMesh(N2,T2,'r');
M=InterfaceMatrix(N1,T1,N2,T2);
Mf=full(M);