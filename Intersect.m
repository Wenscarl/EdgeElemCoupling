function [P,n,M]=Intersect(X,normX,Y,normY)
global isEdgeBasis;
global numEdgea;
global numEdgeb;
% INTERSECT intersection of two triangles and mortar contribution
%   [P,n,M]=Intersect(X,Y); computes for the two given triangles X and
%   Y (point coordinates are stored column-wise, in counter clock
%   order) the points P where they intersect, in n the indices of
%   which neighbors of X are also intersecting with Y, and the local
%   mortar matrix M of contributions of the P1 elements X on the P1
%   element Y. The numerical challenges are handled by including
%   points on the boundary and removing duplicates at the end.

[P,n]=EdgeIntersections(X,Y);
P1=PointsOfXInY(X,Y);
if size(P1,2)>1                      % if two or more interior points
  n=[1 1 1];                         % the triangle is candidate for all 
end                                  % neighbors
P=[P P1];
P=[P PointsOfXInY(Y,X)];
P=SortAndRemoveDoubles(P);  % sort counter clock wise
if isEdgeBasis ==1
    M = zeros(8,16);
else
    M=zeros(3,3);
end
if size(P,2)>0
    % old version
%   for j=2:size(P,2)-1                % compute interface matrix
%       if isEdgeBasis
%           M=M+MortarIntEdgeElement(P(:,[1 j j+1]),X,Y);% ian modified
%       else
%           M=M+MortarInt(P(:,[1 j j+1]),X,Y); 
%       end    
%   end;
    % new version
    
barry = [mean(P(1,:));mean(P(2,:))];
for j=1:size(P,2)-1
    localTri = [barry P(:,[j j+1])];
    if isEdgeBasis
        M=M+MortarIntEdgeElement(localTri,X,normX,Y,normY);% ian modified
    else
        M=M+MortarInt(localTri,X,Y); 
    end
end
     localTri = [barry P(:,[end 1])];
    if isEdgeBasis
        M=M+MortarIntEdgeElement(localTri,X,normX,Y,normY);% ian modified
    else
        M=M+MortarInt(localTri,X,Y); 
    end
    
  patch(P(1,:),P(2,:),'g')           % draw intersection for illustration
  H=line([P(1,:) P(1,1)],[P(2,:),P(2,1)]);
  set(H,'LineWidth',3,'Color','k');
  pause(0)
end