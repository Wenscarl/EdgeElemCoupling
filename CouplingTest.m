clear;close all;
global cj
global rootDIR
global isEdgeBasis;
% test the advancing front program
global fig coldim rowdim;
% global flag1 flag2 % indicate two tri's normal consistancy
% global jindexa jindexb eindexb norm_a norm_b
cj = i;
isEdgeBasis=true;
rootDIR = '/home/wang/workspace/MortarCoupling';
% rootDIR = '/home/ian/Documents/MortarCoupling/Working';
addpath(genpath(rootDIR));


%% readin geometry info
skip = 'x';
[Na Ta jIdxa eIdxa] = readGeometry('boxA');% Ta: 0 based index
[Nb Tb jIdxb eIdxb] = readGeometry('boxB');

[N1,T1,numE1] = mapto2d(Na,Ta,skip);
[N2,T2,numE2] = mapto2d(Nb,Tb,skip);

%% determine which is source (j only)
source = 'a';

%%

% pre-known info, common normal direction
switch skip
  case char('x')
    normal = [1,0,0];
  case char('y')
    normal = [0,1,0];
  case char('z')
   normal = [0,0,1];
  otherwise
      disp('Error, no such touching id is found! Try again!') 
end

%%

fig=figure(1);
clf
set(fig,'DoubleBuffer','on');

[N1,T1,flag1]=regulateTri(normal,N1,T1);% change the order of T1 according to normal, make it as normal!!
[N2,T2,flag2]=regulateTri(normal,N2,T2);

PlotMesh(N1,T1,'b');
PlotMesh(N2,T2,'r');

% T2(e,j)->T1(j)

switch source
    case char('a')
        title('blue: src;red rcv');
        jIdxa = jIdxa - numE1*2 - 2*length(T1(:,1));
        coldim = 76;
        rowdim = 230;
        M = InterfaceMatrix(N2,T2,flag2,eIdxb+1,jIdxb+1,N1,T1,flag1,eIdxa+1,jIdxa+1);
    case char('b')
        title('blue: rcv;red src');
        jIdxb = jIdxb - numE2*2 - 2*length(T2(:,1));
%         coldim = 76;
        coldim = 460;% for plot
        rowdim = 38;
        M = InterfaceMatrix(N1,T1,flag1,eIdxa+1,jIdxa+1,N2,T2,flag2,eIdxb+1,jIdxb+1);
    otherwise
      disp('Error, Wrong source! Try again!') 
end


Mf=full(M);
