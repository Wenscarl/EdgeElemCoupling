function [Ta Na numEdge] = appendNeighbour(unit ,numNode, numTri ,NodeList ,TriList)
Ta = zeros(numTri,6);
Ta(:,1:3) = TriList;
BoundFlag = numTri+1;
numEdge = 0;

EdgeName =  [];
Edgetri1  = [];
Edgetri2  = [];
for i = 1:numTri
    edge1 = sort(TriList(i,1:2));
    edge2 = sort(TriList(i,2:3));
    edge3 = sort(TriList(i,[1,3]));
    edgeLocalList = [edge1;edge2;edge3];
    for j = 1:3
%         key = [int2str(edgeLocalList(j,1)),'b',int2str(edgeLocalList(j,2))];
        key = [edgeLocalList(j,1) edgeLocalList(j,2)];
        if(~isempty(EdgeName))
            ri = find(EdgeName(:,1)==key(1) & EdgeName(:,2)==key(2));
        else
            ri = [];
        end
%         [ri,ci] = find(strcmp(EdgeList.name,key));
        if isempty(ri)
            EdgeName = [EdgeName;key];
            Edgetri1 = [Edgetri1; [i,j]];
            Edgetri2 = [Edgetri2; [-1 -1]];
            numEdge = numEdge+1;
        else
            Edgetri2(ri,:) = [i,j];
        end
    end
end
% EdgeName
% Edgetri1
% Edgetri2

for i = 1:numEdge
    tri1Id = Edgetri1(i,1);
    tri1LocId = Edgetri1(i,2);
    tri2Id = Edgetri2(i,1);
    tri2LocId = Edgetri2(i,2);
    if tri2Id == -1
        tri2Id = BoundFlag;
    end
    
    Ta(tri1Id,tri1LocId+3) = tri2Id;
    if tri2Id == BoundFlag
        continue;
    else
        Ta(tri2Id,tri2LocId+3) = tri1Id;
    end
    
end

% for i=1:numTri
%     edge1 = sort(TriList(i,1:2));
%     edge2 = sort(TriList(i,2:3));
%     edge3 = sort(TriList(i,[1,3]));
%     edgeLocalList = [edge1;edge2;edge3];
%     for ii=1:3
%         key = [int2str(edgeLocalList(ii,1)),'b',int2str(edgeLocalList(ii,2))];
%         tmp = Edge_ht.get(key);
%         if size(tmp,1)==2
%             Ta(i,ii+3) = sum(tmp)-i;
%         else 
%             Ta(i,ii+3) = BoundFlag;
%         end
%     end
% end
%         
%         
Na = NodeList';
Ta(:,1:3) = Ta(:,1:3)+1;
% % 
% Edge_list = Edge_ht.keys;
% while( Edge_list.hasNext )% word_list.hasNext为真说明下一个元素存在，否则已遍历到列表尾
%     word = Edge_list.nextElement;% 获取下一个关键字
%     fprintf('%s : %d\n', word, Edge_ht.get(word)); % 使用 ht.get(key) 可得到key对应的value，即 value = ht.get(key)
% end